msgid ""
msgstr ""
"Project-Id-Version: TAO v2.6-RC03\n"
"PO-Revision-Date: 2014-06-20T16:26:44\n"
"Last-Translator: TAO Translation Team <translation@tao.lu>\n"
"MIME-Version: 1.0\n"
"Language: sv-SE\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Class saved"
msgstr "Klass sparad"

msgid "delete"
msgstr "ta bort"

msgid "Edit subject"
msgstr "Redigera testtagare"

msgid "Edit subject class"
msgstr "Redigera testtagarklass"

msgid "export"
msgstr "exportera"

msgid "import"
msgstr "importera"

msgid "lists"
msgstr "listor"

#, tao-public
msgid "Manage test takers"
msgstr "Hantera testtagare"

msgid "meta data"
msgstr "metadata"

msgid "move"
msgstr "flytta"

msgid "new class"
msgstr "ny klass"

msgid "new test taker"
msgstr "ny testtagare"

#, tao-public
msgid "Record and manage test takers."
msgstr ""

msgid "search"
msgstr "sök"

msgid "Test taker saved"
msgstr "Testtagare sparad"

msgid "Test takers"
msgstr "Testtagare"

msgid "Test takers library"
msgstr "Testtagararkiv"

msgid "Test takers may be assigned to sets according to the relevant assessment context (Human resources management, education, edumetric monitoring, etc.)."
msgstr "TEsttagare kan knytas till placeringar enligt det relevanta utvärderingssammanhanget (human resource management, utbildning etc.)."

#, tao-public
msgid "Test-takers"
msgstr ""

msgid "The Test takers module allows to record and manage  test takers."
msgstr "Testtagarmodulen låter dig registrera och hantera testtagare"

