msgid ""
msgstr ""
"Project-Id-Version: TAO v2.6-RC03\n"
"PO-Revision-Date: 2014-06-20T16:26:44\n"
"Last-Translator: TAO Translation Team <translation@tao.lu>\n"
"MIME-Version: 1.0\n"
"Language: de-DE\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Class saved"
msgstr "Klasse gespeichert"

msgid "delete"
msgstr "löschen"

msgid "Edit subject"
msgstr "Testperson bearbeiten"

msgid "Edit subject class"
msgstr "Klasse"

msgid "export"
msgstr "Export"

msgid "import"
msgstr ""

msgid "lists"
msgstr ""

#, tao-public
msgid "Manage test takers"
msgstr "Testpersonen verwalten"

msgid "meta data"
msgstr "Meta-Daten"

msgid "move"
msgstr ""

msgid "new class"
msgstr "neue Klasse"

msgid "new test taker"
msgstr ""

#, tao-public
msgid "Record and manage test takers."
msgstr ""

msgid "search"
msgstr "suchen"

msgid "Test taker saved"
msgstr ""

msgid "Test takers"
msgstr "Testpersonen"

msgid "Test takers library"
msgstr "Testpersonen-Bibliothek"

msgid "Test takers may be assigned to sets according to the relevant assessment context (Human resources management, education, edumetric monitoring, etc.)."
msgstr "Testpersonen können entsprechend dem relevanten Assessment-Kontext (Personalverwaltung, Bildung, Edumetrisches Monitoring usw,) zusammengefasst werden."

#, tao-public
msgid "Test-takers"
msgstr ""

msgid "The Test takers module allows to record and manage  test takers."
msgstr "Das Testpersonen-Modul ermöglicht das Speichern und Verwalten von Testpersonen."

